﻿using System.Collections.Generic;
using System.Web.UI.WebControls;
using HotCar.BLL.Abstract;
using HotCar.Entities;
using HotCar.Repositories.Abstract;

namespace HotCar.BLL
{
    public class ApplManager: IApplManager
    {
        #region Fields

        private readonly IPassengerRepository _passengerRepository;
        private readonly ITripRepository _tripRepository;
        private readonly IUserRepository _userRepository;

        #endregion


        #region Constructor

        public ApplManager(IPassengerRepository passengerRepository, ITripRepository tripRepository, IUserRepository userRepository)
        {
            _passengerRepository = passengerRepository;
            _tripRepository = tripRepository;
            _userRepository = userRepository;
        }

        #endregion


        #region IApplManager

        public void SignToTrip(Passenger passenger)
        {
            _passengerRepository.SignPassengerToTrip(passenger);
        }

        public bool ConfirmApplsForUser(Passenger passenger)
        {
            Trip currentTrip = _tripRepository.GetTripById(passenger.TripId);
            if (currentTrip != null)
            {
                if (!passenger.IsConfirmed)
                {
                    if (currentTrip.AvailablePlacesCount >= passenger.CountReservedSeats)
                    {
                        _passengerRepository.ConfirmPassengerToTrip(passenger.Id);
                        currentTrip.AvailablePlacesCount -= passenger.CountReservedSeats;
                        _tripRepository.SetAvailablePlacesCount(currentTrip.Id, (byte) currentTrip.AvailablePlacesCount);

                        return true;
                    }
                }
            }

            return false;
        }

        public List<Passenger> GetUnConfirmedPassengerToTrip(int tripId)
        {
            List<Passenger> pass = _passengerRepository.GetAllPassengersByTripId(tripId);
            for (int i = 0; i < pass.Count; i++)
            {
                if (pass[i].IsConfirmed)
                {
                    pass.RemoveAt(i--);
                }
            }
            SignPassengerToUser(pass);
            return pass;
        }

        public List<Passenger> GetConfirmedPassengerToTrip(int tripId)
        {
            List<Passenger> pass = _passengerRepository.GetAllPassengersByTripId(tripId);
            for (int i = 0; i < pass.Count; i++)
            {
                if (!pass[i].IsConfirmed)
                {
                    pass.RemoveAt(i--);
                }
            }
            SignPassengerToUser(pass);

            return pass;
        }

        #endregion


        #region Helpers

        private void SignPassengerToUser(List<Passenger> passengers)
        {
            for (int i = 0; i < passengers.Count; i++)
            {
                passengers[i].UserInfo = _userRepository.GetUserByLogin(passengers[i].UserInfo.Login);
            }
        }

        #endregion
    }
}
