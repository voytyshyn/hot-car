﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GoogleMapsApi.Entities.Common;
using GoogleMapsApi.Entities.Directions.Request;
using GoogleMapsApi.Entities.Directions.Response;
using GoogleMapsApi.Entities.Geocoding.Request;
using GoogleMapsApi.Entities.Geocoding.Response;
using HotCar.BLL.Abstract;
using HotCar.Entities;
using HotCar.Repositories.Sql;
using HotCar.Repositories.Abstract;

namespace HotCar.BLL
{
    public class TripManager : ITripManager
    {
        #region Private Fields

        private readonly ITripRepository _tripRepository;
        private readonly IPassengerRepository _passengerRepository;
        private readonly IUserRepository _userRepository;
        private readonly ICommentRepository _commentRepository;

        #endregion

        #region Constructor

        public TripManager(ITripRepository tripRepository, IPassengerRepository passengerRepository, IUserRepository userRepository, ICommentRepository commentRepository)
        {          
            this._tripRepository = tripRepository;
            this._passengerRepository = passengerRepository;
            this._userRepository = userRepository;
            this._commentRepository = commentRepository;


        }

        #endregion

        #region Interface Implementation

        public void AddNew(Trip trip)
        {
            var tripId = this.AddTrip(trip);
            this.AddRoute(trip.RouteLocations, tripId);
        }

        public List<Trip> GetPassengerAvailableTrips(String from, String to, double tolerance = 1000)
        {
            List<Trip> driverTrips = this._tripRepository.GetAllPassengerAvailableTrips(DateTime.Now.AddMinutes(15));
            List<Trip> passengerAvailableTrips = new List<Trip>();

            for (int a = 0; a < driverTrips.Count; a++)
            {
                if (driverTrips[a].IsOnRoute(from, to, tolerance))
                {
                    passengerAvailableTrips.Add(driverTrips[a]);
                }
            }

            this.GetLocationsAddressess(passengerAvailableTrips);

            for (int a = 0; a < passengerAvailableTrips.Count; a++)
            {
                passengerAvailableTrips[a].Passangers = this.GetPassengersByTripId(passengerAvailableTrips[a].Id);
            }

            return passengerAvailableTrips;
        }
  
        public List<Trip> GetActiveUserTrips(string userLogin, bool isConfirmed)
        {
            DateTime now = DateTime.Now.AddMinutes(1);

            List<Trip> asDriver = this._tripRepository.GetActualTripsByDriverLogin(userLogin, now);
            List<Trip> asPassenger = this._tripRepository.GetActualTripsByPassengerLogin(userLogin, now, isConfirmed);

            List<Trip> actual = new List<Trip>(asDriver);
            actual.AddRange(asPassenger);

            this.SignLocationAndPassengerToTrips(actual);

            return actual;
        }

        public List<Trip> GetOutDatedUserTrips(string userLogin, bool isConfirmed)
        {
            DateTime now = DateTime.Now.AddMinutes(1);

            List<Trip> asDriver = this._tripRepository.GetOutDatedTripsByDriverLogin(userLogin, now);
            List<Trip> asPassenger = this._tripRepository.GetOutDatedTripsByPassengerLogin(userLogin, now, isConfirmed);

            List<Trip> outDated = new List<Trip>(asDriver);
            outDated.AddRange(asPassenger);
            
            this.SignLocationAndPassengerToTrips(outDated);

            return outDated;
        }

        public Trip GetTripById(int tripId) 
        {
            Trip trip = this._tripRepository.GetTripById(tripId);
            trip.Passangers = this.GetPassengersByTripId(tripId);
            this.GetLocationsAddressess(new List<Trip>() { trip });

            return trip;
        }

        public List<Passenger> GetPassengersByTripId(int tripId)
        {
            List<Passenger> passengers = this._passengerRepository.GetAllPassengersByTripId(tripId);
            for (int a = 0; a < passengers.Count; a++)
            {
                passengers[a].UserInfo = this._userRepository.GetUserByLogin(passengers[a].UserInfo.Login);

                var rating = this._commentRepository.GetPassengerRating(passengers[a].UserInfo.Id);
                passengers[a].UserInfo.Rating = rating.Count > 0 ? Math.Round(rating.Average(), 2) : 0;
            }

            return passengers;
        }

        public List<Trip> GetActiveTripsForDriverByLogin(string driverLogin)
        {
            DateTime now = DateTime.Now.AddMinutes(1);
            List<Trip> asDriver = this._tripRepository.GetActualTripsByDriverLogin(driverLogin, now);
            this.SignLocationAndPassengerToTrips(asDriver);

            return asDriver;
        }

        public void ClearPassengersFromTrip(Trip trip, bool isConfirmed)
        {
            for (int i = 0; i < trip.Passangers.Count; i++)
            {
                if (trip.Passangers[i].IsConfirmed == isConfirmed)
                {
                    trip.Passangers.RemoveAt(i--);
                }
            }
        }

        #endregion

        #region Helpers

        private int AddTrip(Trip trip)
        {
            return this._tripRepository.InsertTrip(trip);
        }

        private void AddRoute(List<LocationInfo> location, int tripId)
        {
            foreach (var locationInfo in location)
            {
                this._tripRepository.AddRoute(locationInfo, tripId);
            }
        }

        private void GetLocationsAddressess(List<Trip> trips)
        {
            for (int a = 0; a < trips.Count; a++)
            {
                trips[a].GetLocationAddresses();
            }
        }

        private void SignLocationAndPassengerToTrips(List<Trip> trips)
        {
            this.GetLocationsAddressess(trips);

            for (int i = 0; i < trips.Count; i++)
            {
                trips[i].Passangers = this.GetPassengersByTripId(trips[i].Id);
            }
        }

        #endregion
    }
}
