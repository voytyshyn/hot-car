﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Configuration;

namespace HotCar.BLL.Services
{
    public class SmsService : ISmsService
    {
        #region Fields

        private readonly NameValueCollection _settingsCollection;

        #endregion


        #region Methods

        public void SendSms(string phone, string message)
        {
            SendSms(phone, message, sender: this.Sender);
        }

        #endregion


        #region Constructor

        public SmsService(NameValueCollection settingsCollection)
        {
            this._settingsCollection = settingsCollection;
        }

        #endregion


        #region ConfigurationFields

        private bool UsePost
        {
            get { return Convert.ToBoolean(WebConfigurationManager.AppSettings["smsPost"]); }
        }

        private bool UseHttps
        {
            get { return Convert.ToBoolean(WebConfigurationManager.AppSettings["smsHttps"]); }
        }

        private string Charset
        {
            get { return WebConfigurationManager.AppSettings["smsCharset"]; }
        }

        private string Login
        {
            get { return WebConfigurationManager.AppSettings["smsLogin"]; }
        }

        private string Password
        {
            get { return WebConfigurationManager.AppSettings["smsPassword"]; }
        }

        private string Sender
        {
            get { return WebConfigurationManager.AppSettings["smsSender"]; }
        }

        #endregion


        #region Helpers

        private string[] SendSms(string phones, string message, int translit = 0, string time = "", int id = 0, int format = 0, string sender = "", string query = "")
        {
            string[] formats = { "flash=1", "push=1", "hlr=1", "bin=1", "bin=2", "ping=1", "mms=1", "mail=1", "call=1" };

            string[] m = SmsSendCmd("send", "cost=3&phones=" + Urlencode(phones)
                            + "&mes=" + Urlencode(message) + "&id=" + id.ToString() + "&translit=" + translit.ToString()
                            + (format > 0 ? "&" + formats[format - 1] : "") + (sender != "" ? "&sender=" + Urlencode(sender) : "")
                            + (time != "" ? "&time=" + Urlencode(time) : "") + (query != "" ? "&" + query : ""), null);

            return m;
        }

        private string[] SmsSendCmd(string cmd, string arg, string[] files = null)
        {
            arg = "login=" + Urlencode(this.Login) + "&psw=" + Urlencode(this.Password) + "&fmt=1&charset=" + this.Charset + "&" + arg;

            string url = (this.UseHttps ? "https" : "http") + "://smsc.ru/sys/" + cmd + ".php" + (this.UsePost ? "" : "?" + arg);

            string ret;
            int i = 0;
            HttpWebRequest request;
            StreamReader sr;
            HttpWebResponse response;

            do
            {
                if (i > 0)
                    System.Threading.Thread.Sleep(2000 + 1000 * i);

                if (i == 2)
                    url = url.Replace("://smsc.ru/", "://www2.smsc.ru/");

                request = (HttpWebRequest)WebRequest.Create(url);

                if (this.UsePost)
                {
                    request.Method = "POST";

                    string postHeader, boundary = "----------" + DateTime.Now.Ticks.ToString("x");
                    byte[] postHeaderBytes, boundaryBytes = Encoding.ASCII.GetBytes("--" + boundary + "--\r\n"), tbuf;
                    StringBuilder sb = new StringBuilder();
                    int bytesRead;

                    byte[] output = new byte[0];

                    if (files == null)
                    {
                        request.ContentType = "application/x-www-form-urlencoded";
                        output = Encoding.UTF8.GetBytes(arg);
                        request.ContentLength = output.Length;
                    }
                    else
                    {
                        request.ContentType = "multipart/form-data; boundary=" + boundary;

                        string[] par = arg.Split('&');
                        int fl = files.Length;

                        for (int pcnt = 0; pcnt < par.Length + fl; pcnt++)
                        {
                            sb.Clear();

                            sb.Append("--");
                            sb.Append(boundary);
                            sb.Append("\r\n");
                            sb.Append("Content-Disposition: form-data; name=\"");

                            bool pof = pcnt < fl;
                            String[] nv = new String[0];

                            if (pof)
                            {
                                sb.Append("File" + (pcnt + 1));
                                sb.Append("\"; filename=\"");
                                sb.Append(Path.GetFileName(files[pcnt]));
                            }
                            else
                            {
                                nv = par[pcnt - fl].Split('=');
                                sb.Append(nv[0]);
                            }

                            sb.Append("\"");
                            sb.Append("\r\n");
                            sb.Append("Content-Type: ");
                            sb.Append(pof ? "application/octet-stream" : "text/plain; charset=\"" + this.Charset + "\"");
                            sb.Append("\r\n");
                            sb.Append("Content-Transfer-Encoding: binary");
                            sb.Append("\r\n");
                            sb.Append("\r\n");

                            postHeader = sb.ToString();
                            postHeaderBytes = Encoding.UTF8.GetBytes(postHeader);

                            output = Concatb(output, postHeaderBytes);

                            if (pof)
                            {
                                FileStream fileStream = new FileStream(files[pcnt], FileMode.Open, FileAccess.Read);

                                // Write out the file contents
                                byte[] buffer = new Byte[checked((uint)Math.Min(4096, (int)fileStream.Length))];

                                bytesRead = 0;
                                while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
                                {
                                    tbuf = buffer;
                                    Array.Resize(ref tbuf, bytesRead);

                                    output = Concatb(output, tbuf);
                                }
                            }
                            else
                            {
                                byte[] vl = Encoding.UTF8.GetBytes(nv[1]);
                                output = Concatb(output, vl);
                            }

                            output = Concatb(output, Encoding.UTF8.GetBytes("\r\n"));
                        }
                        output = Concatb(output, boundaryBytes);

                        request.ContentLength = output.Length;
                    }

                    Stream requestStream = request.GetRequestStream();
                    requestStream.Write(output, 0, output.Length);
                }

                try
                {
                    response = (HttpWebResponse)request.GetResponse();

                    sr = new StreamReader(response.GetResponseStream());
                    ret = sr.ReadToEnd();
                }
                catch (WebException)
                {
                    ret = "";
                }
            }
            while (ret == "" && ++i < 4);

            char delim = ',';

            if (cmd == "status")
            {
                string[] par = arg.Split('&');

                for (i = 0; i < par.Length; i++)
                {
                    string[] lr = par[i].Split("=".ToCharArray(), 2);

                    if (lr[0] == "id" && lr[1].IndexOf("%2c") > 0) 
                        delim = '\n';
                }
            }

            return ret.Split(delim);
        }

        private string Urlencode(string str)
        {
            if (this.UsePost) return str;

            return HttpUtility.UrlEncode(str);
        }

        private byte[] Concatb(byte[] farr, byte[] sarr)
        {
            int opl = farr.Length;

            Array.Resize(ref farr, farr.Length + sarr.Length);
            Array.Copy(sarr, 0, farr, opl, sarr.Length);

            return farr;
        }

        #endregion
    }
}