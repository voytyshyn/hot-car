﻿namespace HotCar.BLL.Services
{
    public interface ISmsService
    {
        void SendSms(string phone, string message);
    }
}
