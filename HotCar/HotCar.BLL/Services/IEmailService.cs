﻿namespace HotCar.BLL.Services
{
    public interface IEmailService
    {
        string Email { get; }
        void Send(string to, string message, string subject);
        string GetMessageStatus();
    }
}
