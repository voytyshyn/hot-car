﻿using System;

namespace HotCar.Entities
{
    public class Comment
    {
        public int Id { get; set; }
        public DateTime Time { get; set; }
        public string Text { get; set; }
        public bool IsPassenger { get; set; }
        public int ParticipantId { get; set; }
        public int TripId { get; set; }
        public double Rating { get; set; }
        public User UserInfo{ get; set; }

        public Comment()
        {
            this.UserInfo = new User();
        }
    }
}
