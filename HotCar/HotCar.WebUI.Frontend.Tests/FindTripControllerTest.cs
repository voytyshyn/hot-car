﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.UI.WebControls;
using HotCar.BLL.Abstract;
using HotCar.Entities;
using HotCar.WebUI.Frontend.Code;
using HotCar.WebUI.Frontend.Controllers;
using HotCar.WebUI.Frontend.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace HotCar.WebUI.Frontend.Tests
{
    [TestClass]
    public class FindTripControllerTest
    {
        #region Fields

        private Mock<ITripManager> _tripManagerMock;
        private Mock<IUsersManager> _userManagerMock;
        private Mock<ISecurityManager> _securityManagerMock;
        private FindTripController _findTripController;

        #endregion

        #region Test methods

        [TestInitialize]
        public void Initialize()
        {
            this._tripManagerMock = new Mock<ITripManager>();
            this._userManagerMock = new Mock<IUsersManager>();
            this._securityManagerMock = new Mock<ISecurityManager>();

            this._findTripController = new FindTripController(this._tripManagerMock.Object,
                this._securityManagerMock.Object, this._userManagerMock.Object);
        }

        [TestMethod]
        public void IndexGetTest()
        {
            ViewResult viewResult = this._findTripController.Index() as ViewResult;

            Assert.AreEqual("Index", viewResult.ViewName);
        }

        [TestMethod]
        public void IndexPostTest()
        {
            Mock<HttpRequestBase> httpRequestMock = new Mock<HttpRequestBase>();
            httpRequestMock.SetupGet(x => x.Headers).Returns(new System.Net.WebHeaderCollection { { "X-Requested-With", "XMLHttpRequest" } });

            Mock<HttpContextBase> httpContextBaseMock = new Mock<HttpContextBase>();
            httpContextBaseMock.SetupGet(x => x.Request).Returns(httpRequestMock.Object);

            Mock<ControllerContext> controllerContextMock = new Mock<ControllerContext>();
            controllerContextMock.Setup(r => r.HttpContext.Request["X-Requested-With"]).Returns("");
            controllerContextMock.Setup((cc) => cc.HttpContext.Session[SessionKeys.TRIPS]).Returns(null);

            FindTripModel findTripModel = new FindTripModel();
            findTripModel.From = "Lviv";
            findTripModel.To = "Ternopil";

            this._findTripController.ControllerContext = controllerContextMock.Object;

            ViewResult viewResult = this._findTripController.Index(findTripModel) as ViewResult;

            Assert.AreEqual(0, viewResult.ViewBag.NoSeatsCount);
            Assert.AreEqual(0, viewResult.ViewBag.PhotoCount);
            Assert.AreEqual(0, viewResult.ViewBag.MaxPage);
            Assert.AreEqual(0, viewResult.ViewBag.Page);
            Assert.AreEqual(0, viewResult.ViewBag.Count);
            Assert.AreEqual(0, viewResult.ViewBag.Data.Count);
            Assert.AreEqual("Results", viewResult.ViewName);
        }

        [TestMethod]
        public void ResultsNoFiltersNoAjaxTest()
        {
            Trip trip = new Trip();
            trip.Id = 1;
            trip.TripTime = DateTime.Now;
            trip.AdditionalInfo = "additional";
            trip.CostOneSeat = 50;
            trip.AvailablePlacesCount = 5;
            List<Trip> trips = new List<Trip>();
            trips.Add(trip);

            Mock<HttpRequestBase> httpRequestMock = new Mock<HttpRequestBase>();
            httpRequestMock.SetupGet(x => x.Headers).Returns(new System.Net.WebHeaderCollection { { "X-Requested-With", "XMLHttpRequest" } });

            Mock<HttpContextBase> httpContextBaseMock = new Mock<HttpContextBase>();
            httpContextBaseMock.SetupGet(x => x.Request).Returns(httpRequestMock.Object);

            Mock<ControllerContext> controllerContextMock = new Mock<ControllerContext>();
            controllerContextMock.Setup(r => r.HttpContext.Request["X-Requested-With"]).Returns("");
            controllerContextMock.Setup((cc) => cc.HttpContext.Session[SessionKeys.TRIPS]).Returns(trips);

            this._findTripController.ControllerContext = controllerContextMock.Object;

            ViewResult viewResult = this._findTripController.Results() as ViewResult;

            Assert.AreEqual(0, viewResult.ViewBag.NoSeatsCount);
            Assert.AreEqual(0, viewResult.ViewBag.PhotoCount);
            Assert.AreEqual(0, viewResult.ViewBag.MaxPage);
            Assert.AreEqual(0, viewResult.ViewBag.Page);
            Assert.AreEqual(1, viewResult.ViewBag.Count);
            Assert.AreEqual(trip, viewResult.ViewBag.Data[0]);
            Assert.AreEqual("Results", viewResult.ViewName);
        }

        [TestMethod]
        public void ResultsNoFiltersNoAjax2Test()
        {
            Trip trip = new Trip();
            trip.Id = 1;
            trip.TripTime = DateTime.Now;
            trip.AdditionalInfo = "additional";
            trip.CostOneSeat = 50;
            trip.AvailablePlacesCount = 5;
            List<Trip> trips = new List<Trip>();
            trips.Add(trip);
            trips.Add(trip);
            trips.Add(trip);
            trips.Add(trip);
            trips.Add(trip);

            Mock<HttpRequestBase> httpRequestMock = new Mock<HttpRequestBase>();
            httpRequestMock.SetupGet(x => x.Headers).Returns(new System.Net.WebHeaderCollection { { "X-Requested-With", "XMLHttpRequest" } });

            Mock<HttpContextBase> httpContextBaseMock = new Mock<HttpContextBase>();
            httpContextBaseMock.SetupGet(x => x.Request).Returns(httpRequestMock.Object);

            Mock<ControllerContext> controllerContextMock = new Mock<ControllerContext>();
            controllerContextMock.Setup(r => r.HttpContext.Request["X-Requested-With"]).Returns("");
            controllerContextMock.Setup((cc) => cc.HttpContext.Session[SessionKeys.TRIPS]).Returns(trips);

            this._findTripController.ControllerContext = controllerContextMock.Object;

            ViewResult viewResult = this._findTripController.Results() as ViewResult;

            Assert.AreEqual(0, viewResult.ViewBag.NoSeatsCount);
            Assert.AreEqual(0, viewResult.ViewBag.PhotoCount);
            Assert.AreEqual(1, viewResult.ViewBag.MaxPage);
            Assert.AreEqual(0, viewResult.ViewBag.Page);
            Assert.AreEqual(5, viewResult.ViewBag.Count);
            Assert.AreEqual(4, viewResult.ViewBag.Data.Count);
            Assert.AreEqual("Results", viewResult.ViewName);
        }

        [TestMethod]
        public void ResultsNoFiltersNoAjax3Test()
        {
            Trip trip = new Trip();
            trip.Id = 1;
            trip.TripTime = DateTime.Now;
            trip.AdditionalInfo = "additional";
            trip.CostOneSeat = 50;
            trip.AvailablePlacesCount = 5;
            List<Trip> trips = new List<Trip>();
            trips.Add(trip);
            trips.Add(trip);
            trips.Add(trip);
            trips.Add(trip);
            
            Mock<HttpRequestBase> httpRequestMock = new Mock<HttpRequestBase>();
            httpRequestMock.SetupGet(x => x.Headers).Returns(new System.Net.WebHeaderCollection { { "X-Requested-With", "XMLHttpRequest" } });

            Mock<HttpContextBase> httpContextBaseMock = new Mock<HttpContextBase>();
            httpContextBaseMock.SetupGet(x => x.Request).Returns(httpRequestMock.Object);

            Mock<ControllerContext> controllerContextMock = new Mock<ControllerContext>();
            controllerContextMock.Setup(r => r.HttpContext.Request["X-Requested-With"]).Returns("");
            controllerContextMock.Setup((cc) => cc.HttpContext.Session[SessionKeys.TRIPS]).Returns(trips);

            this._findTripController.ControllerContext = controllerContextMock.Object;

            ViewResult viewResult = this._findTripController.Results() as ViewResult;

            Assert.AreEqual(0, viewResult.ViewBag.NoSeatsCount);
            Assert.AreEqual(0, viewResult.ViewBag.PhotoCount);
            Assert.AreEqual(0, viewResult.ViewBag.MaxPage);
            Assert.AreEqual(0, viewResult.ViewBag.Page);
            Assert.AreEqual(4, viewResult.ViewBag.Count);
            Assert.AreEqual(4, viewResult.ViewBag.Data.Count);
            Assert.AreEqual("Results", viewResult.ViewName);
        }
        [TestMethod]
        public void ResultsNoFiltersIsAjaxTest()
        {
            Trip trip = new Trip();
            trip.Id = 1;
            trip.TripTime = DateTime.Now;
            trip.AdditionalInfo = "additional";
            trip.CostOneSeat = 50;
            trip.AvailablePlacesCount = 5;
            List<Trip> trips = new List<Trip>();
            trips.Add(trip);
            trips.Add(trip);
            trips.Add(trip);
            trips.Add(trip);
            trips.Add(trip);
            trips.Add(trip);

            Mock<HttpRequestBase> httpRequestMock = new Mock<HttpRequestBase>();
            httpRequestMock.SetupGet(x => x.Headers).Returns(new System.Net.WebHeaderCollection { { "X-Requested-With", "XMLHttpRequest" } });

            Mock<HttpContextBase> httpContextBaseMock = new Mock<HttpContextBase>();
            httpContextBaseMock.SetupGet(x => x.Request).Returns(httpRequestMock.Object);

            Mock<ControllerContext> controllerContextMock = new Mock<ControllerContext>();
            controllerContextMock.Setup(r => r.HttpContext.Request["X-Requested-With"]).Returns("XMLHttpRequest");
            controllerContextMock.Setup((cc) => cc.HttpContext.Session[SessionKeys.TRIPS]).Returns(trips);

            this._findTripController.ControllerContext = controllerContextMock.Object;

            PartialViewResult partialViewResult = this._findTripController.Results(1) as PartialViewResult;

            Assert.AreEqual(0, partialViewResult.ViewBag.NoSeatsCount);
            Assert.AreEqual(0, partialViewResult.ViewBag.PhotoCount);
            Assert.AreEqual(1, partialViewResult.ViewBag.MaxPage);
            Assert.AreEqual(1, partialViewResult.ViewBag.Page);
            Assert.AreEqual(trips.Count, partialViewResult.ViewBag.Count);
            Assert.AreEqual(2, partialViewResult.ViewBag.Data.Count);
            Assert.AreEqual("ResultsPartial", partialViewResult.ViewName);
        }

        [TestMethod]
        public void ResultsAreFiltersIsAjaxTest()
        {
            List<Trip> trips = new List<Trip>();
            for (int a = 0; a < 6; a++)
            {
                Trip trip = new Trip();
                trip.Id = a + 1;
                trip.TripTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10 + a * 2, a * 3, 0);
                trip.AdditionalInfo = "additional";
                trip.CostOneSeat = 50;
                trip.AvailablePlacesCount = 5;
                trips.Add(trip);
            }


            Mock<HttpRequestBase> httpRequestMock = new Mock<HttpRequestBase>();
            httpRequestMock.SetupGet(x => x.Headers).Returns(new System.Net.WebHeaderCollection { { "X-Requested-With", "XMLHttpRequest" } });

            Mock<HttpContextBase> httpContextBaseMock = new Mock<HttpContextBase>();
            httpContextBaseMock.SetupGet(x => x.Request).Returns(httpRequestMock.Object);

            Mock<ControllerContext> controllerContextMock = new Mock<ControllerContext>();
            controllerContextMock.Setup(r => r.HttpContext.Request["X-Requested-With"]).Returns("XMLHttpRequest");
            controllerContextMock.Setup((cc) => cc.HttpContext.Session[SessionKeys.TRIPS]).Returns(trips);

            this._findTripController.ControllerContext = controllerContextMock.Object;

            FiltersModel filtersModel = new FiltersModel();
            filtersModel.FromHour = 10 + 1;
            filtersModel.ToHour = 10 + 8;
            filtersModel.HideWithNoPhoto = false;
            filtersModel.HideWithNoSeats = true;
            filtersModel.Date = DateTime.Now.ToString("d").Replace('.', '/');

            PartialViewResult partialViewResult = this._findTripController.Results(0, filtersModel) as PartialViewResult;

            Assert.AreEqual(0, partialViewResult.ViewBag.NoSeatsCount);
            Assert.AreEqual(0, partialViewResult.ViewBag.PhotoCount);
            Assert.AreEqual(0, partialViewResult.ViewBag.MaxPage);
            Assert.AreEqual(0, partialViewResult.ViewBag.Page);
            Assert.AreEqual(3, partialViewResult.ViewBag.Count);
            Assert.AreEqual(3, partialViewResult.ViewBag.Data.Count);
            Assert.AreEqual("ResultsPartial", partialViewResult.ViewName);
        }

        [TestMethod]
        public void ResultsAreFiltersPhotoNotNullIsAjaxTest()
        {
            List<Trip> trips = new List<Trip>();
            for (int a = 0; a < 6; a++)
            {
                Trip trip = new Trip();
                trip.Id = a + 1;
                trip.TripTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10 + a * 2, a * 3, 0);
                trip.AdditionalInfo = "additional";
                trip.CostOneSeat = 50;
                trip.AvailablePlacesCount = 5;
                trips.Add(trip);
            }


            Mock<HttpRequestBase> httpRequestMock = new Mock<HttpRequestBase>();
            httpRequestMock.SetupGet(x => x.Headers).Returns(new System.Net.WebHeaderCollection { { "X-Requested-With", "XMLHttpRequest" } });

            Mock<HttpContextBase> httpContextBaseMock = new Mock<HttpContextBase>();
            httpContextBaseMock.SetupGet(x => x.Request).Returns(httpRequestMock.Object);

            Mock<ControllerContext> controllerContextMock = new Mock<ControllerContext>();
            controllerContextMock.Setup(r => r.HttpContext.Request["X-Requested-With"]).Returns("XMLHttpRequest");
            controllerContextMock.Setup((cc) => cc.HttpContext.Session[SessionKeys.TRIPS]).Returns(trips);

            this._findTripController.ControllerContext = controllerContextMock.Object;

            FiltersModel filtersModel = new FiltersModel();
            filtersModel.FromHour = 10 + 1;
            filtersModel.ToHour = 10 + 8;
            filtersModel.HideWithNoPhoto = true;
            filtersModel.HideWithNoSeats = true;
            filtersModel.Date = DateTime.Now.ToString("d").Replace('.', '/');

            PartialViewResult partialViewResult = this._findTripController.Results(0, filtersModel) as PartialViewResult;

            Assert.AreEqual(0, partialViewResult.ViewBag.NoSeatsCount);
            Assert.AreEqual(0, partialViewResult.ViewBag.PhotoCount);
            Assert.AreEqual(0, partialViewResult.ViewBag.MaxPage);
            Assert.AreEqual(0, partialViewResult.ViewBag.Page);
            Assert.AreEqual(0, partialViewResult.ViewBag.Count);
            Assert.AreEqual(0, partialViewResult.ViewBag.Data.Count);
            Assert.AreEqual("ResultsPartial", partialViewResult.ViewName);
        }


        [TestMethod]
        public void TripDetailTest()
        {
            Trip trip = new Trip();
            this._tripManagerMock.Setup((tmm) => tmm.GetTripById(It.IsAny<int>())).Returns(trip);
            this._securityManagerMock.Setup((smm) => smm.GetName()).Returns("aaa");
            this._userManagerMock.Setup((umm) => umm.GetUser("aaa")).Returns(new User());
            ViewResult viewResult = this._findTripController.TripDetail(1) as ViewResult;

            Assert.AreEqual("TripDetail", viewResult.ViewName);
            Assert.AreNotEqual(null, viewResult.Model);
        }

        #endregion
    }
}
