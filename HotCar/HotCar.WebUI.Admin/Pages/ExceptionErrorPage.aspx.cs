﻿using System;
using System.Globalization;
using System.Web.UI;
using HotCar.Entities.Enums;
using HotCar.WebUI.Admin.Code.ExceptionWorking;
using HotCar.WebUI.Admin.Code.Keys;

namespace HotCar.WebUI.Admin.Pages
{
    public partial class ExceptionErrorPage : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            try
            {
                Exception ex = Server.GetLastError().GetBaseException();
                ExceptionInfo exceptionInfo = new ExceptionInfo(ex);

                lblWhat.Text = exceptionInfo.GetInfo.WhatHappenedInfo;
                lblWhy.Text = exceptionInfo.GetInfo.WhyHappenedInfo;
                lblSuggestion.Text = exceptionInfo.GetInfo.SuggestionInfo;
            }
            catch
            { }
        }
    }
}