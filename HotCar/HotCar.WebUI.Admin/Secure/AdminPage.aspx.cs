﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using HotCar.BLL;
using HotCar.BLL.Services;
using HotCar.Entities;
using HotCar.Entities.Enums;
using HotCar.Repositories.Sql;
using HotCar.WebUI.Admin.Code.Keys;
using Ninject;
using Ninject.Web;

namespace HotCar.WebUI.Admin.Secure
{
    public partial class AdminPage : PageBase
    {
        #region Inject

        [Inject]
        public IEmailService EmailService { get; set; }

        private static IEmailService _emailService;

        #endregion

        #region ASP.NET Page Life Cycle Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                _emailService = this.EmailService;
                this.lblUsersListName.Text = "Усі користувачі";
            }
            
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (this.Session[SessionKeys.BIND_METHOD] != null)
            {
                this.odsGetUsers.SelectMethod = (string) this.Session[SessionKeys.BIND_METHOD];
            }
            this.GridViewUsers.DataBind();
        }

        #endregion

        #region GridView Events

        protected void GridViewUsers_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.GridViewUsers.PageIndex = e.NewPageIndex;                
        }

        protected void GridViewUsers_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridViewRow row = this.GridViewUsers.Rows[e.NewEditIndex];
            UserRoles editRole = (UserRoles)(Enum.Parse(typeof(UserRoles),
                ((Label)row.FindControl("lblUserRole")).Text));

            if (this.User.IsInRole(UserRoles.Administrator.ToString()))
            {
                if (editRole == UserRoles.Administrator || editRole == UserRoles.Master)
                {
                    String permission = "";
                    String alert = "";
                    permission = "Обмеження прав";
                    alert = "У вас немає належних прав";
                    

                    ScriptManager.RegisterStartupScript(this, typeof(Page), permission, "<script>alert('" + alert + "');</script>", false);

                    e.NewEditIndex = -1;
                }

                else
                {
                    this.GridViewUsers.EditIndex = e.NewEditIndex;
                }
            }

            else
            {
                this.GridViewUsers.EditIndex = e.NewEditIndex;
            }
        }

        protected void GridViewUsers_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            var user = new User();
            GridViewRow row = this.GridViewUsers.Rows[e.RowIndex];

            user.Id = Convert.ToInt32(((Label)this.GridViewUsers.Rows[e.RowIndex].FindControl("lblUserIdu")).Text);
            user.Login = ((Label)this.GridViewUsers.Rows[e.RowIndex].FindControl("lblUserLoginu")).Text;
            user.FirstName = ((TextBox)this.GridViewUsers.Rows[e.RowIndex].FindControl("txtUserNameu")).Text;
            user.SurName = ((TextBox)this.GridViewUsers.Rows[e.RowIndex].FindControl("txtUserSurnameu")).Text;
            user.Role = (UserRoles)(Enum.Parse(typeof(UserRoles),
                ((DropDownList)this.GridViewUsers.Rows[e.RowIndex].FindControl("ddlUserRole")).SelectedValue));
            user.Phone = ((TextBox)this.GridViewUsers.Rows[e.RowIndex].FindControl("txtPhoneu")).Text;
            user.Mail = ((TextBox)this.GridViewUsers.Rows[e.RowIndex].FindControl("txtMailu")).Text;
            user.AboutMe = ((TextBox)this.GridViewUsers.Rows[e.RowIndex].FindControl("txtAboutu")).Text;
            user.Inactive = Convert.ToBoolean(((TextBox)this.GridViewUsers.Rows[e.RowIndex].FindControl("txtInactiveu")).Text);
            string birthday = ((TextBox)this.GridViewUsers.Rows[e.RowIndex].FindControl("txtBirthdayu")).Text;
            user.Birthday = new DateTime(Convert.ToInt32(birthday.Split('.', '/', '-')[2]),
                    Convert.ToInt32(birthday.Split('.', '/', '-')[1]),
                    Convert.ToInt32(birthday.Split('.', '/', '-')[0]));

            var role = this.User.IsInRole(UserRoles.Administrator.ToString()) ?
               UserRoles.Administrator : UserRoles.Master;

            if ((int)role <= (int)user.Role)
            {
                String permission = String.Empty;
                String alert = String.Empty;
                permission = "Обмеження прав";
                alert = "У вас немає належних прав";

                ScriptManager.RegisterStartupScript(this, typeof(Page), "", "<script>alert('" + alert + "');</script>", false);
                this.GridViewUsers.EditIndex = -1;
            }
            else
            {
                var usersManager = new UsersManager(new UserRepository(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString), new SecurityManager(new UserRepository(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString)));
                usersManager.UpdateUserInfo(user);

                this.GridViewUsers.EditIndex = -1;
            }                                                      
        }

        protected void GridViewUsers_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            this.GridViewUsers.EditIndex = -1;           
        }

        protected void ddlGridViewPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.GridViewUsers.PageSize = Convert.ToInt32(this.ddlGridViewPageSize.Items[this.ddlGridViewPageSize.SelectedIndex].Value);
        }

        #endregion

        #region Select Users Events

        protected void btnAdmins_Click(object sender, EventArgs e)
        {
            this.odsGetUsers.SelectParameters.Clear();
            this.Session[SessionKeys.BIND_METHOD] = "GetUsersByRole";
            this.odsGetUsers.SelectParameters.Add("roles", TypeCode.String, UserRoles.Administrator.ToString());
            this.lblUsersListName.Text = "Адміністратори";
        }

        protected void btnSimpleUsers_Click(object sender, EventArgs e)
        {
            this.Session[SessionKeys.BIND_METHOD] = "GetUsersByRole";
            this.odsGetUsers.SelectParameters.Clear();
            this.odsGetUsers.SelectParameters.Add("roles", TypeCode.String, UserRoles.User.ToString());
            this.lblUsersListName.Text = "Прості користувачі";
        }

        protected void btnInactiveUsers_Click(object sender, EventArgs e)
        {
            this.Session[SessionKeys.BIND_METHOD] = "GetInactiveUsers";
            this.odsGetUsers.SelectParameters.Clear();
            this.lblUsersListName.Text = "Неактивні користувачі";
           
        }

        protected void btnAllUsers_Click(object sender, EventArgs e)
        {
            this.Session[SessionKeys.BIND_METHOD] = "GetUsersByRole";
            this.odsGetUsers.SelectParameters.Clear();
            this.odsGetUsers.SelectParameters.Add("roles", TypeCode.String, "all");
            this.lblUsersListName.Text = "Усі користувачі";
            
        }

        #endregion

        #region WebMethods

        [WebMethod]
        public static string SendMail(string[] receivers, string subject, string message)
        {
            if (receivers.Length != 0)
            {
                foreach (var receiver in receivers)
                {
                    _emailService.Send(to: receiver, subject: subject, message: message);
                }
                return "Повідомлення надіслано";
            }
            
            return "Помилка при надсиланні повідомлення!";
        }

        [WebMethod]
        public static string GetInputMessagInfo()
        {
            return _emailService.GetMessageStatus();
        }

        #endregion WebMethods

        #region Helpers

        private void GridViewBinding(List<User> users )
        {
            this.GridViewUsers.DataSource = users;
            this.GridViewUsers.DataBind();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            this.Session[SessionKeys.BIND_METHOD] = "SearchUsers";
            this.odsGetUsers.SelectParameters.Clear();
            this.odsGetUsers.SelectParameters.Add("searchRequest", TypeCode.String, this.txtSearch.Text);         
        }

        protected void btnLockUsers_Click(object sender, EventArgs e)
        {
            var manager = new UsersManager(new UserRepository(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString), new SecurityManager(new UserRepository(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString)));
            var role = this.User.IsInRole(UserRoles.Administrator.ToString()) ?
                UserRoles.Administrator : UserRoles.Master;

            manager.UsersLock(this.CheckedList(), role);          
        }

        protected void btnUnlockUser_Click(object sender, EventArgs e)
        {
            var manager = new UsersManager(new UserRepository(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString), new SecurityManager(new UserRepository(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString)));           
            manager.UsersUnlock(this.CheckedList());
            
        }
        
        private IDictionary<int, string> CheckedList()
        {
            var checkedUsers = new Dictionary<int, string>();

            foreach (GridViewRow row in this.GridViewUsers.Rows)
            {
                var chBox = ((CheckBox)row.FindControl("chkRow"));
                if (chBox.Checked)
                {
                    int uId = Convert.ToInt32(((Label)row.FindControl("lblUserId")).Text);
                    string uRole = ((Label)row.FindControl("lblUserRole")).Text;
                    checkedUsers.Add(uId, uRole);
                    chBox.Checked = false;
                }
            }

            return checkedUsers;
        }

        #endregion
    }
}