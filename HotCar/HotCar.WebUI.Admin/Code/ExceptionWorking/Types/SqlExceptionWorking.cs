﻿namespace HotCar.WebUI.Admin.Code.ExceptionWorking.Types
{
    public class SqlExceptionWorking : IWorkingExeption
    {
        public string WhatHappenedInfo
        {
            get { return "Помилка з'єднання з базою даних"; }
        }

        public string WhyHappenedInfo
        {
            get { return "база даних не доступна"; }
        }

        public string SuggestionInfo
        {
            get { return "почекати декілька хвилин і спробувати ще раз"; }
        }
    }
}