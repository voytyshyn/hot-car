﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HotCar.WebUI.Frontend.Models
{
    public class LoginModel
    {
         [Required(ErrorMessage = "Введіть логін")]
        public string UserName { get; set; }

         [Required(ErrorMessage = "Введіть пароль")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public bool RememberMe { get; set; }
    }
}