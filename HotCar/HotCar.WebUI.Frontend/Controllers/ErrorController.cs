﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HotCar.WebUI.Frontend.Controllers
{
    public class ErrorController : Controller
    {
        //
        // GET: /Error/
        [HttpGet]
        public ActionResult ServerError()
        {
            return View("ServerError");
        }

        [HttpGet]
        public ActionResult NotFound()
        {
            return View("NotFound");
        }

        [HttpGet]
        public ActionResult AccessDenied()
        {
            return View("AccessDenied");
        }
	}
}