﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HotCar.Entities;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using HotCar.BLL.Abstract;
using HotCar.WebUI.Frontend.Models;
using HotCar.WebUI.Frontend.Code;

namespace HotCar.WebUI.Frontend.Controllers
{
    public class AccountController : Controller
    {
        #region Private Fields

        private ISecurityManager _securityManager;
        private IUsersManager _usersManager;

        #endregion

        #region Constructors

        public AccountController(ISecurityManager secure, IUsersManager usersManager)
        {
            this._securityManager = secure;
            this._usersManager = usersManager;
        }

        #endregion

        #region Actions

        [HttpGet]
        public ActionResult Login(string returnUrl)
        {
            this.ViewBag.ReturnUrl = returnUrl;
            return this.View("Login");
        }
       
        public ActionResult Logout()
        {
            this._securityManager.SignOut();
            return this.RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (this.ModelState.IsValid)
            {
                string username = model.UserName;
                string password = model.Password;
                if (this._securityManager.Authentication(username, password))
                {
                    return this.Redirect(returnUrl ?? this.Url.Action("Index", "Home"));
                }
                else
                {
                    this.ModelState.AddModelError(String.Empty,"Неправильний логін або пароль");                  
                    return this.View("Login");
                }

            }
            else
            {
                return this.View("Login");
            }          
        }

        [HttpGet]
        public ActionResult Register(string returnUrl)
        {
            this.ViewBag.ReturnUrl = returnUrl;
            return this.View("Register");
        }

        [HttpPost]
        public ActionResult Register(RegisterModel model, string returnUrl)
        {
            if (this.ModelState.IsValid)
            {
                var user = new User();
                user.Login = model.UserName;
                user.FirstName = model.FirstName;
                user.SurName = model.LastName;
                user.Password = model.Password;
                user.Mail = model.Email;
                user.Phone = model.Phone;
                user.Birthday = new DateTime(Convert.ToInt32(model.BirthdayString.Split('.', '/', '-')[2]),
                    Convert.ToInt32(model.BirthdayString.Split('.', '/', '-')[1]),
                    Convert.ToInt32(model.BirthdayString.Split('.', '/', '-')[0]));

                bool? createAccount = this._securityManager.CreateAccount(user);

                if (createAccount == true)
                {
                    return this.Redirect(returnUrl ?? this.Url.Action("Index", "Home", new { infoAlert = "Ваша сторінка успішно зареєстрована!"}));
                }

                else if (createAccount == null)
                {
                    this.ModelState.AddModelError(String.Empty, "Така електронна пошта вже зареєстрована");
                    return this.View("Register");
                }

                else
                {
                    this.ModelState.AddModelError(String.Empty, "Такий логін вже зареєстрований");
                    return this.View("Register");
                }
            }

             else
             {
                 this.ModelState.AddModelError(String.Empty, "Невірні дані");
                 return this.View("Register");
             }                   
        }

        public ActionResult UserPhoto(string login)
        {
            UserPhoto userPhoto = this._usersManager.GetUserPhoto(login);
            if (userPhoto.Photo != null)
            {
                return this.File(userPhoto.Photo, MimeMapping.GetMimeMapping(userPhoto.FileExtension));
            }
            else
            {
                string fileName = this.Server.MapPath("~/Content/images/no-user-photo.png");
                return this.File(fileName, MimeMapping.GetMimeMapping(fileName));
            }

        }

        #endregion
    }
}