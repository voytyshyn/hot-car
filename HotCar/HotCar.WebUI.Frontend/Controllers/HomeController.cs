﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HotCar.BLL.Services;
using HotCar.WebUI.Frontend.Models;
using HotCar.WebUI.Frontend.Code;


namespace HotCar.WebUI.Frontend.Controllers
{
    public class HomeController : Controller
    {
        #region Fields

        private readonly IEmailService _emailService;

        #endregion

        #region Constructors

        public HomeController(IEmailService emailService)
        {
            this._emailService = emailService;
        }

        #endregion

        #region Actions

        public ActionResult Index(string infoAlert = null)
        {
            ViewBag.Alert = infoAlert != null ? infoAlert : "";
            return View("Index");
        }

        public ActionResult HowItWorks()
        {
            return View("HowItWorks");
        }

        public ActionResult FAQ()
        {
            return View("FAQ");
        }

        public ActionResult Contact()
        {
            return View("Contact");
        }

        [HttpPost]
        public JsonResult Contact(MailMessageModel mail = null)
        {
            if (mail != null)
            {
                string formattedMessage = String.Empty;
                formattedMessage += "Ім'я користувача: " + mail.Name + "\n";
                formattedMessage += "Ел. адреса користувача: " + mail.MailAddress + "\n";
                formattedMessage += "Текст повідомлення: " + mail.Message;

                this._emailService.Send(to: this._emailService.Email, message: formattedMessage, subject: "Запитання від користувача");

                return Json("Success");
            }
            return Json("Failed");
        }

        #endregion
    }
}