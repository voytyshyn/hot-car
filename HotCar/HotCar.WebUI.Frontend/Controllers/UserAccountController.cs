﻿using System;
using System.Collections.Generic;
using System.Configuration.Internal;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Helpers;
using System.Web.Mvc;
using HotCar.BLL.Abstract;
using HotCar.BLL.Services;
using HotCar.Entities;
using HotCar.WebUI.Frontend.Code;
using HotCar.WebUI.Frontend.Code.ControllerExtensions;
using HotCar.WebUI.Frontend.Models;


namespace HotCar.WebUI.Frontend.Controllers
{
    public class UserAccountController : Controller
    {
        #region ApplyPassengerStatusStringConst

        private const string APPLY_PLACES_ERROR = "немає стільки вільних місць";

        private const string APPLY_DRIVER_ERROR = "тільки водій на поїздці може підтвердити заявку на цю поїздку";

        private const string APPLY_SUCCESS = "пасажир успішно доданий";

        private const string APPLY_PASSENGER_ERROR = "такий пасажир не є учасником поїздки";

        private const string APPLY_UNEXPECTED_ERROR = "невідома помилка; будь ласка зверніться до адміністрації сайту";

        private const string APPLY_SUCCESS_INFO = "Ваша заявка підтверджена! водій: ";

        private const string APPLY_SUCCESS_SUBJECT = "[підтвердження заявки]";

        #endregion

        #region Fields

        private readonly ITripManager _tripManager;
        private readonly IUsersManager _userManager;
        private readonly ICommentManager _commentManager;
        private readonly IApplManager _applManager;
        private readonly ISecurityManager _securityManager;
        private readonly ISmsService _smsService;
        private readonly IEmailService _emailService;

        #region Avatar Settings

        private const int AvatarStoredWidth = 100;  // ToDo - Change the size of the stored avatar image
        private const int AvatarStoredHeight = 100; // ToDo - Change the size of the stored avatar image
        private const int AvatarScreenWidth = 400;  // ToDo - Change the value of the width of the image on the screen

        private static string TempFolder = WebConfigurationManager.AppSettings["TempFolderPath"];
        private static string MapTempFolder = "~" + TempFolder;     

        private readonly string[] _imageFileExtensions = { ".jpg", ".png", ".gif", ".jpeg" };

        #endregion

        #endregion

        #region Constructor

        public UserAccountController(IUsersManager userManager, ITripManager tripManager,
            ICommentManager commentManager, IApplManager applManager,
            ISecurityManager securityManager, ISmsService smsService, IEmailService emailService)
        {
            this._userManager = userManager;
            this._tripManager = tripManager;
            this._commentManager = commentManager;
            this._applManager = applManager;
            this._securityManager = securityManager;
            this._smsService = smsService;
            this._emailService = emailService;
        }

        #endregion
        
        #region Web Actions

        [Authorize]
        public ActionResult Index()
        {
            UserModel userModel = new UserModel();
            userModel.UserInfo = this._userManager.GetUser(this.User.Identity.Name);

            return View(userModel);
        }

        [Authorize]
        [HttpGet]
        public ActionResult GetOutDatedTrips()
        {
            UserModel userModel = new UserModel();
            userModel.Trips = this._tripManager.GetOutDatedUserTrips(User.Identity.Name, true);

            return this.GetHtmlTripContent(userModel);
        }

        [Authorize]
        [HttpGet]
        public ActionResult GetActiveTrips()
        {
            UserModel userModel = new UserModel();
            userModel.Trips = this._tripManager.GetActiveUserTrips(User.Identity.Name, true);

            return this.GetHtmlTripContent(userModel);
        }

        [Authorize]
        [HttpGet]
        public ActionResult GetMyComments()
        {
            UserModel userModel = new UserModel();
            userModel.UserInfo = this._userManager.GetUser(this._securityManager.GetName());
            userModel.Comments = this._commentManager.GetAllMyComments(userModel.UserInfo.Id);

            return this.GetHtmlCommentsContent(userModel);
        }

        [Authorize]
        [HttpGet]
        public ActionResult GetCommentsAboutMe()
        {
            UserModel userModel = new UserModel();
            userModel.UserInfo = this._userManager.GetUser(this.User.Identity.Name);
            userModel.Comments = this._commentManager.GetAllAboutMeComments(userModel.UserInfo.Id);

            return this.GetHtmlCommentsContent(userModel);
        }

        [Authorize]
        [HttpGet]
        public ActionResult GetNewApplsToTrip()
        {
            UserModel userModel = new UserModel();
            userModel.Trips = _tripManager.GetActiveTripsForDriverByLogin(User.Identity.Name);

            int countNewAppls = 0;
            for (int i = 0; i < userModel.Trips.Count; i++)
            {
                _tripManager.ClearPassengersFromTrip(userModel.Trips[i], true);

                if (userModel.Trips[i].Passangers.Count == 0)
                {
                    userModel.Trips.RemoveAt(i--);
                    continue;
                }

                countNewAppls += userModel.Trips[i].Passangers.Count;
            }
            
            return Json(new
            {
                count = countNewAppls,
                htmlContent = this.RenderPartialViewToString("Partials/ApplPartial", userModel)
            }, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ApplyPassengerToTrip(int tripId, string passengerLogin)
        {
            Trip currentTrip = _tripManager.GetTripById(tripId);

            Passenger pass = _applManager.GetUnConfirmedPassengerToTrip(tripId).FirstOrDefault(
                p => String.Compare(p.UserInfo.Login, passengerLogin, true) == 0);

            string message;
            bool success = ChackPassengerToTrip(currentTrip, pass, out message);

            if (success)
            {
                if (!_applManager.ConfirmApplsForUser(pass))
                {
                    message = APPLY_UNEXPECTED_ERROR;
                    success = false;
                }
                else
                {
                    try
                    {
                        string name = currentTrip.Driver.FirstName + " " + currentTrip.Driver.SurName;
                        InformPassengerForApplying(pass.UserInfo.Mail, pass.UserInfo.Phone,
                        APPLY_SUCCESS_INFO + currentTrip.Driver.Phone + "(" + name + ")", 
                        APPLY_SUCCESS_SUBJECT);
                    }
                    catch (Exception)
                    {
                    }
                }
            }

            return Json(new
            {
                success, 
                message = message
            });
        }

        #region Avatar Actions

        [ValidateAntiForgeryToken]
        public ActionResult Upload(IEnumerable<HttpPostedFileBase> files)
        {
            if (files == null || !files.Any()) return Json(new { success = false, errorMessage = "No file uploaded." });
            var file = files.FirstOrDefault();  // get ONE only
            if (file == null || !IsImage(file)) return Json(new { success = false, errorMessage = "File is of wrong format." });
            if (file.ContentLength <= 0) return Json(new { success = false, errorMessage = "File cannot be zero length." });
            var webPath = GetTempSavedFilePath(file);
            return Json(new { success = true, fileName = webPath.Replace("/", "\\") }); // success
        }

        [HttpPost]
        public ActionResult Save(string t, string l, string h, string w, string fileName)
        {
            try
            {
                // Calculate dimensions
                var top = Convert.ToInt32(t.Replace("-", "").Replace("px", ""));
                var left = Convert.ToInt32(l.Replace("-", "").Replace("px", ""));
                var height = Convert.ToInt32(h.Replace("-", "").Replace("px", ""));
                var width = Convert.ToInt32(w.Replace("-", "").Replace("px", ""));

                // Get file from temporary folder
                var fn = Path.Combine(Server.MapPath(MapTempFolder), Path.GetFileName(fileName));
                // ...get image and resize it, ...
                var img = new WebImage(fn);
                img.Resize(width, height);
                // ... crop the part the user selected, ...
                img.Crop(top, left, img.Height - top - AvatarStoredHeight, img.Width - left - AvatarStoredWidth);
                // ... delete the temporary file,...         
                System.IO.File.Delete(fn);
                // ... and save the new one.                          
                bool res = this._userManager.UploadPhoto(img.GetBytes(), img.ImageFormat);
           
                return Json(new { success = res});
            }
            catch (Exception ex)
            {
                return Json(new { success = false, errorMessage = "Unable to upload file.\nERRORINFO: " + ex.Message });
            }
        }

        #endregion

        #region Setting Actions

        [Authorize]
        public ActionResult ChangeFirstName(String firstName)
        {
            UserModel userModel = new UserModel();
            userModel.UserInfo = this._userManager.GetUser(this.User.Identity.Name);
            userModel.UserInfo.FirstName = firstName;
            bool updated = this._userManager.UpdateUserInfo(userModel.UserInfo);

            return Json(new { success = updated });
        }

        [Authorize]
        public ActionResult ChangeSurName(String surName)
        {
            UserModel userModel = new UserModel();
            userModel.UserInfo = this._userManager.GetUser(this.User.Identity.Name);
            userModel.UserInfo.SurName = surName;
            bool updated = this._userManager.UpdateUserInfo(userModel.UserInfo);

            return Json(new { success = updated });
        }

        [Authorize]
        public ActionResult ChangeBirthday(String birthday)
        {
            DateTime dateTime = new DateTime(Convert.ToInt32(birthday.Split('.', '/', '-')[2]),
                    Convert.ToInt32(birthday.Split('.', '/', '-')[1]),
                    Convert.ToInt32(birthday.Split('.', '/', '-')[0]));

            UserModel userModel = new UserModel();
            userModel.UserInfo = this._userManager.GetUser(this.User.Identity.Name);
            userModel.UserInfo.Birthday = dateTime;
            bool updated = this._userManager.UpdateUserInfo(userModel.UserInfo);

            return Json(new { success = updated });
        }

        [Authorize]
        public ActionResult ChangeMail(String mail)
        {
            UserModel userModel = new UserModel();
            userModel.UserInfo = this._userManager.GetUser(this.User.Identity.Name);
            userModel.UserInfo.Mail = mail;
            bool updated = this._userManager.IsMailDBValid(mail);

            if (!updated)
            {
                updated = this._userManager.UpdateUserInfo(userModel.UserInfo);
            }

            else
            {
                updated = false;
            }

            return Json(new { success = updated });
        }

        [Authorize]
        public ActionResult ChangePhone(String phone)
        {
            UserModel userModel = new UserModel();
            userModel.UserInfo = this._userManager.GetUser(this.User.Identity.Name);
            userModel.UserInfo.Phone = phone;
            bool updated = this._userManager.UpdateUserInfo(userModel.UserInfo);

            return Json(new { success = updated });
        }

        [Authorize]
        public ActionResult ChangePassword(String oldPassword, String newPassword, String newPasswordConfirm)
        {
            UserModel userModel = new UserModel();
            userModel.UserInfo = this._userManager.GetUser(this.User.Identity.Name);
            string currentPassword = Encoding.Default.GetString(this._userManager.GetUserPassword(this.User.Identity.Name));

            bool updated = this._securityManager.CheckPassword(currentPassword, oldPassword);

            if (!updated || !this._securityManager.CheckPassword(this._securityManager.EncryptPassword(newPassword), newPasswordConfirm))
            {
                updated = false;
            }

            if (updated)
            {
                updated = this._userManager.UpdateUserPassword(userModel.UserInfo.Id, this._securityManager.EncryptPassword(newPassword));
            }

            return Json(new { success = updated });
        }

        [Authorize]
        public ActionResult ChangeAbout(String about)
        {
            UserModel userModel = new UserModel();
            userModel.UserInfo = this._userManager.GetUser(this.User.Identity.Name);
            userModel.UserInfo.AboutMe = about;
            bool updated = this._userManager.UpdateUserInfo(userModel.UserInfo);

            return Json(new { success = updated });
        }

        #endregion

        #endregion

        #region Helpers

        private void InformPassengerForApplying(string email, string phone, string message, string subject)
        {
            this._smsService.SendSms(phone, message);
            this._emailService.Send(email, message, subject);
        }

        [Authorize]
        private ActionResult GetHtmlTripContent(UserModel userModel)
        {
            return Json(new
                {
                    count = userModel.Trips.Count,
                    htmlContent = this.RenderPartialViewToString("Partials/TripPartial", userModel)
                }, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        private ActionResult GetHtmlCommentsContent(UserModel userModel)
        {
            return Json(new
            {
                count = userModel.Comments.Count,
                htmlContent = this.RenderPartialViewToString("Partials/CommentPartial", userModel) 
            }, JsonRequestBehavior.AllowGet);
        }

        private bool ChackPassengerToTrip(Trip currentTrip, Passenger pass, out string message)
        {
            message = APPLY_SUCCESS;

            if (pass != null)
            {
                if (String.Compare(currentTrip.Driver.Login, User.Identity.Name, true) == 0)
                {
                    if (pass.CountReservedSeats <= currentTrip.AvailablePlacesCount)
                    {
                        return true;
                    }
                    else
                    {
                        message = APPLY_PLACES_ERROR;
                    }
                }
                else
                {
                    message = APPLY_DRIVER_ERROR;
                }
            }
            else
            {
                message = APPLY_PASSENGER_ERROR;
            }

            return false;
        }

        #region Avatar Helpers

        private bool IsImage(HttpPostedFileBase file)
        {
            if (file == null) return false;
            return file.ContentType.Contains("image") ||
                _imageFileExtensions.Any(item => file.FileName.EndsWith(item, StringComparison.OrdinalIgnoreCase));
        }

        private string GetTempSavedFilePath(HttpPostedFileBase file)
        {
            // Define destination
            var serverPath = HttpContext.Server.MapPath(TempFolder);
            if (Directory.Exists(serverPath) == false)
            {
                Directory.CreateDirectory(serverPath);
            }

            // Generate unique file name
            var fileName = Path.GetFileName(file.FileName);
            fileName = SaveTemporaryAvatarFileImage(file, serverPath, fileName);

            // Clean up old files after every save
            CleanUpTempFolder(1);
            return Path.Combine(TempFolder, fileName);
        }

        private static string SaveTemporaryAvatarFileImage(HttpPostedFileBase file, string serverPath, string fileName)
        {
            var img = new WebImage(file.InputStream);
            var ratio = img.Height / (double)img.Width;
            img.Resize(AvatarScreenWidth, (int)(AvatarScreenWidth * ratio));

            var fullFileName = Path.Combine(serverPath, fileName);
            if (System.IO.File.Exists(fullFileName))
            {
                System.IO.File.SetAttributes(fullFileName, FileAttributes.Normal);
                System.IO.File.Delete(fullFileName);
            }          
            img.Save(fullFileName);
            return Path.GetFileName(img.FileName);
        }

        private void CleanUpTempFolder(int hoursOld)
        {
            try
            {
                var currentUtcNow = DateTime.UtcNow;
                var serverPath = HttpContext.Server.MapPath("/Temp");
                if (!Directory.Exists(serverPath)) return;
                var fileEntries = Directory.GetFiles(serverPath);
                foreach (var fileEntry in fileEntries)
                {
                    var fileCreationTime = System.IO.File.GetCreationTimeUtc(fileEntry);
                    var res = currentUtcNow - fileCreationTime;
                    if (res.TotalHours > hoursOld)
                    {              
                        System.IO.File.Delete(fileEntry);
                    }
                }
            }
            catch
            {
                // Deliberately empty.
            }
        }

        #endregion

        #endregion
    }
}