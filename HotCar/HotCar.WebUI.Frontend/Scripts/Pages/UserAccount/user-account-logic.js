﻿function Content($content) {
	// очистити область
	var clearContent = function (slider) {
		if (slider === true) {
			$content.slideToggle('slow');
		}
		$content.empty();
	}

	// заповнити область даними
	var fillContent = function (htmlData, slider) {
		if (slider === true) {
			$content.slideToggle('slow');
		}
		$content.html(htmlData);
	}

	// очистити і заповнити даними локально
	this.refillContent = function (htmlData, slider) {
		clearContent(slider);
		fillContent(htmlData, slider);
	}

};

function Trips() {
	var actualTrips = {
		count: null,
		htmlData: null
	};

	var outDatedTrips = {
		count: null,
		htmlData: null
	};

	this.getCountActualTrips = function () {
		return actualTrips.count;
	}

	this.getCountOutDatedTrips = function () {
		return outDatedTrips.count;
	}

	this.getActualTripHtml = function () {
		return actualTrips.htmlData;
	}

	this.getOutDatedTripHtml = function () {
		return outDatedTrips.htmlData;
	}

	this.getActualTrips = function (actionWhenDone) {
		$.ajax({
			url: "/UserAccount/GetActiveTrips",
			type: "GET",
			dataType: "json"
		}).done(function (data) {
			actualTrips.count = data.count;
			actualTrips.htmlData = data.htmlContent;
			actionWhenDone();
		});
	}

	this.getOutDatedTrips = function (actionWhenDone) {
		$.ajax({
			url: "/UserAccount/GetOutDatedTrips",
			type: "GET",
			dataType: "json"
		}).done(function (data) {
			outDatedTrips.count = data.count;
			outDatedTrips.htmlData = data.htmlContent;
			actionWhenDone();
		});
	}
}

function Comments() {
	var myComments = {
		count: null,
		htmlData: null
	};

	var aboutMeComments = {
		count: null,
		htmlData: null
	};

	this.getCountMyComments = function () {
		return myComments.count;
	}

	this.getCountCommentsAboutMe = function () {
		return aboutMeComments.count;
	}

	this.getMyCommentsHtml = function () {
		return myComments.htmlData;
	}

	this.getCommentsAboutMeHtml = function () {
		return aboutMeComments.htmlData;
	}

	this.getMyComments = function (actionWhenDone) {
		$.ajax({
			url: "/UserAccount/GetMyComments",
			type: "GET",
			dataType: "json"
		}).done(function (data) {
			myComments.count = data.count;
			myComments.htmlData = data.htmlContent;
			actionWhenDone();
		});
	}

	this.getCommentsAboutMe = function (actionWhenDone) {
		$.ajax({
			url: "/UserAccount/GetCommentsAboutMe",
			type: "GET",
			dataType: "json"
		}).done(function (data) {
			aboutMeComments.count = data.count;
			aboutMeComments.htmlData = data.htmlContent;
			actionWhenDone();
		});
	}
}

function Appl() {
	var newAppl = {
		count: null,
		htmlData: null
	}

	this.getCountNewAppl = function () {
		return newAppl.count;
	}

	this.getApplsHtml = function () {
		return newAppl.htmlData;
	}

	this.getNewAppl = function (actionWhenDone) {
		$.ajax({
			url: "/UserAccount/GetNewApplsToTrip",
			type: "GET",
			dataType: "json"
		}).done(function (data) {
			newAppl.count = data.count,
			newAppl.htmlData = data.htmlContent;
			actionWhenDone();
		});
	}

	this.applyApplToTrip = function (tripId, passengerLogin, actionWhenDone) {
		$.ajax({
			url: "/UserAccount/ApplyPassengerToTrip",
			type: "POST",
			dataType: "json",
			data: {
				tripId: tripId,
				passengerLogin: passengerLogin
			}
		}).done(function (data) {
			actionWhenDone(data.success, data.message);
		});
	}
}
