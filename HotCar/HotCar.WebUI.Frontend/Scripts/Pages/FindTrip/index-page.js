﻿$(function () {
    $('html, body').animate({
        scrollTop: $("#main-content-header").offset().top
    }, 800);

    $("form").bind("keypress", function (e) {
        if (e.keyCode == 13) {
            $("#btnFind").attr('value');
            return false;
        }
    });

    $('#btnFind').click(function () {
        if (!validateForm()) {
            return false;
        }
    });
});

google.maps.event.addDomListener(window, 'load', function () {
    new google.maps.places.Autocomplete(document.getElementById('from-address'));
    new google.maps.places.Autocomplete(document.getElementById('to-address'));
});

function validateForm() {
    clearErrors();
    var alertDiv = $.parseHTML("<div class='alert alert-danger fade in'><button aria-hidden='true' class='close' data-dismiss='alert' type='button'>×</button></div>");
    var result = true;

    $('.validate').each(function () {
        if ($(this).val() == '') {
            $(this).parent('div').addClass('has-error');
            addAlert($(this).closest('.form-group'), $(alertDiv), "Це поле є обов'язковим!");
            result = false;
        }
    });

    return result;
}

function addAlert(afterElem, alertDiv, info) {
    var alert = alertDiv.clone();
    alert.find('button').after(info);
    afterElem.after(alert);
}

function clearErrors() {
    $('.validate').each(function () {
        if ($(this).parent('div').hasClass('has-error')) {
            $(this).parent('div').removeClass('has-error');
            if ($(this).closest('.form-group').next().hasClass('alert')) {
                $(this).closest('.form-group').next().remove();
            }
        }
    });
}