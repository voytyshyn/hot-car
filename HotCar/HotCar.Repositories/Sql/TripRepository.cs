﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using HotCar.Entities;
using HotCar.Repositories.Abstract;

namespace HotCar.Repositories.Sql
{
    public class TripRepository : Repository, IRepository<Trip>, ITripRepository
    {
        #region Constructor

        public TripRepository(string connectionString)
            : base(connectionString)
        {
        }

        #endregion

        #region IRepository

        public Trip SelectInformation(SqlDataReader reader)
        {
            Trip trip = new Trip();

            trip.Id = (int)reader["Id"];
            trip.TripTime = (DateTime)reader["TripTime"];
            trip.CostOneSeat = (decimal) reader["CostOneSeat"];
            trip.AvailablePlacesCount = (int)reader["AvailablePlacesCount"];
            trip.AdditionalInfo = (String) (reader["AdditionalInfo"] != DBNull.Value ? reader["AdditionalInfo"] : null);
            try { trip.Driver.Login = (String)(reader["UserLogin"] != DBNull.Value ? reader["UserLogin"] : null);                                  } catch(IndexOutOfRangeException e) {}
            try { trip.Driver.AboutMe = (String)(reader["AboutMe"] != DBNull.Value ? reader["AboutMe"] : null);                                    } catch(IndexOutOfRangeException e) {}
            try { trip.Driver.FirstName = (String)( reader["FirstName"] != DBNull.Value ? reader["FirstName"] : null);                             } catch(IndexOutOfRangeException e) {}
            try { trip.Driver.SurName = (String) (reader["SurName"] != DBNull.Value ? reader["SurName"] : null);                                   } catch(IndexOutOfRangeException e) {}
            try { trip.Driver.Birthday = (DateTime?)(reader["Birthday"] != DBNull.Value ? reader["Birthday"] : null);                              } catch(IndexOutOfRangeException e) {}
            try { trip.Driver.Phone = (String) (reader["PhoneNumber"] != DBNull.Value ? reader["PhoneNumber"] : null);                             } catch(IndexOutOfRangeException e) {}
            try { trip.Driver.Mail = (String) (reader["Mail"] != DBNull.Value ? reader["Mail"] : null);                                            } catch(IndexOutOfRangeException e) {}
            try { trip.Driver.Photo.Photo = (byte[])(reader["Photo"] != DBNull.Value ? reader["Photo"] : null);                                    } catch(IndexOutOfRangeException e) {}
            try { trip.Driver.Photo.FileExtension = (string)(reader["PhotoFileExtension"] != DBNull.Value ? reader["PhotoFileExtension"] : null);  } catch(IndexOutOfRangeException e) {}
           
            
            return trip;
        }

        public void SelectLocations(SqlDataReader reader, Trip trip)
        {
            trip.RouteLocations.Add(new LocationInfo((double)(reader["Latitude"]), (double)(reader["Longitude"])));
        }

        #endregion

        #region ITripRepository

        public List<Trip> GetAllPassengerAvailableTrips(DateTime dateTime)
        {
            List<Trip> trips = this.GetAllTripsWithoutLocations(dateTime);

            this.AddLocations(trips);

            return trips;
        }

        #region Available Helpers

        private List<Trip> GetAllTripsWithoutLocations(DateTime dateTime)
        {
            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(StoredProcs.GET_ALL_AVAILABLE_TRIPS, connection) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.Add("@Time", SqlDbType.SmallDateTime).Value = dateTime;

                    List<Trip> trips = new List<Trip>();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            trips.Add(this.SelectInformation(reader));
                        }

                        return trips;
                    }
                }
            }
        }

        private void AddLocations(List<Trip> trips)
        {
            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(StoredProcs.GET_LOCATIONS, connection) { CommandType = CommandType.StoredProcedure })
                {
                    for (int a = 0; a < trips.Count; a++)
                    {
                        command.Parameters.Clear();
                        command.Parameters.Add("@id", SqlDbType.Int).Value = trips[a].Id;

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                this.SelectLocations(reader, trips[a]);
                            }
                        }
                    }
                }
            }
        }
        
        #endregion

        public List<Trip> GetActualTripsByDriverLogin(string login, DateTime dateTime)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(StoredProcs.GET_ACTUAL_TRIPS_BY_DRIVER_LOGIN, connection) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.Add("@driverLogin", SqlDbType.NVarChar).Value = login;
                    command.Parameters.Add("@date", SqlDbType.SmallDateTime).Value = dateTime;

                    List<Trip> trips = new List<Trip>();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            trips.Add(SelectInformation(reader));
                        }

                        this.AddLocations(trips);

                        return trips;
                    }
                }
            }
        }


        public List<Trip> GetOutDatedTripsByDriverLogin(string login, DateTime dateTime)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(StoredProcs.GET_OUTDATED_TRIPS_BY_DRIVER_LOGIN, connection) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.Add("@driverLogin", SqlDbType.NVarChar).Value = login;
                    command.Parameters.Add("@date", SqlDbType.SmallDateTime).Value = dateTime;

                    List<Trip> trips = new List<Trip>();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            trips.Add(SelectInformation(reader));
                        }

                        this.AddLocations(trips);

                        return trips;
                    }
                }
            }
        }

        public List<Trip> GetActualTripsByPassengerLogin(string login, DateTime dateTime, bool isConfirmed)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(StoredProcs.GET_ACTUAL_TRIPS_BY_PASSENGER_LOGIN,
                    connection) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.Add("@passengerLogin", SqlDbType.NVarChar).Value = login;
                    command.Parameters.Add("@date", SqlDbType.SmallDateTime).Value = dateTime;
                    command.Parameters.Add("@isConfirmed", SqlDbType.Bit).Value = isConfirmed;

                    List<Trip> trips = new List<Trip>();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            trips.Add(SelectInformation(reader));
                        }

                        this.AddLocations(trips);

                        return trips;
                    }
                }
            }
        }

        public List<Trip> GetOutDatedTripsByPassengerLogin(string login, DateTime dateTime, bool isConfirmed)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(StoredProcs.GET_OUTDATED_TRIPS_BY_PASSENGER_LOGIN,
                    connection) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.Add("@passengerLogin", SqlDbType.NVarChar).Value = login;
                    command.Parameters.Add("@date", SqlDbType.SmallDateTime).Value = dateTime;
                    command.Parameters.Add("@isConfirmed", SqlDbType.Bit).Value = isConfirmed;

                    List<Trip> trips = new List<Trip>();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            trips.Add(SelectInformation(reader));
                        }

                        this.AddLocations(trips);

                        return trips;
                    }
                }
            }
        }

        public List<Trip> GetAllTripsByDriverId(int id)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(StoredProcs.GET_ALL_TRIPS_BY_DRIVER_ID, connection) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.Add("@driverId", SqlDbType.Int).Value = id;

                    List<Trip> trips = new List<Trip>();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            trips.Add(SelectInformation(reader));
                        }

                        this.AddLocations(trips); 

                        return trips;
                    }
                }
            }
        }

        public int GetCountConductedTripsByDriverId(int id)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(StoredProcs.GET_COUNT_CONDUCTED_TRIPS_BY_DRIVER_ID,
                    connection) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.Add("@driverId", SqlDbType.Int).Value = id;

                    return (int) command.ExecuteScalar();
                }
            }
        }

        public List<Trip> GetAllTripsByPassengerId(int id)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(StoredProcs.GET_ALL_TRIPS_BY_PASSENGER_ID,
                    connection) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.Add("@passengerId", SqlDbType.Int).Value = id;

                    List<Trip> trips = new List<Trip>();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            trips.Add(SelectInformation(reader));
                        }

                        this.AddLocations(trips); 

                        return trips;
                    }
                }
            }
        }

        public int GetCountConductedTripsByPassengerId(int id)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(StoredProcs.GET_COUNT_CONDUCTED_TRIPS_BY_PASSENGER_ID,
                    connection) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.Add("@passengerId", SqlDbType.Int).Value = id;

                    return (int) command.ExecuteScalar();
                }
            }
        }

        public bool DeleteTripById(int id)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(StoredProcs.DELETE_TRIP_BY_ID, connection) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.Add("@id", SqlDbType.Int).Value = id;

                    return Convert.ToBoolean(command.ExecuteNonQuery());
                }
            }
        }

        public bool UpdateTripById(Trip trip)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(StoredProcs.UPDATE_TRIP_BY_ID, connection) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.AddWithValue("@id", trip.Id);
                    command.Parameters.AddWithValue("@ownerId", trip.Driver.Id);                 
                    command.Parameters.AddWithValue("@availablePlacesCount", trip.AvailablePlacesCount);
                    command.Parameters.AddWithValue("@tripTime", trip.TripTime);
                    command.Parameters.AddWithValue("@costOneSeat", trip.CostOneSeat);

                    return Convert.ToBoolean(command.ExecuteNonQuery());
                }
            }
        }

        public int InsertTrip(Trip tripInfo)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(StoredProcs.INSERT_TRIP, connection) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.AddWithValue("@ownerLogin", tripInfo.Driver.Login);
                    command.Parameters.AddWithValue("@placeCount", tripInfo.AvailablePlacesCount);                 
                    command.Parameters.AddWithValue("@tripTime", tripInfo.TripTime);
                    command.Parameters.AddWithValue("@costOneSeat", tripInfo.CostOneSeat);
                    command.Parameters.AddWithValue("@additionalInfo", tripInfo.AdditionalInfo ?? (object)DBNull.Value);

                    return Convert.ToInt32(command.ExecuteScalar());
                }
            }
            
        }

        public bool AddRoute(LocationInfo location, int id)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(StoredProcs.ADD_ROUTE, connection) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.AddWithValue("@tripId", id);
                    command.Parameters.AddWithValue("@longitude ", location.Longitude);
                    command.Parameters.AddWithValue("@latitude", location.Latitude);
                  

                    return Convert.ToBoolean(command.ExecuteNonQuery());
                }
            }

        }

        public Trip GetTripById(int tripId)     //added
        {
            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(StoredProcs.GET_TRIP_BY_ID, connection) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.Add("@id", SqlDbType.Int).Value = tripId;

                    Trip trip = null;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            trip = this.SelectInformation(reader);
                        }

                        this.AddLocations(new List<Trip>() { trip });
                        return trip;
                    }
                }
            }
        }

	public void SetAvailablePlacesCount(int tripId, byte availablePlacesCount)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(StoredProcs.SET_AVAILABLE_PLACES_COUNT, connection) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.AddWithValue("@tripId", tripId);
                    command.Parameters.AddWithValue("@availablePlacesCount", availablePlacesCount);

                    command.ExecuteNonQuery();
                }
            }
        }

        #endregion

    }
}
