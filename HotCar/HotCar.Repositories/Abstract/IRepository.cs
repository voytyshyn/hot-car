﻿using System.Data.SqlClient;

namespace HotCar.Repositories.Abstract
{
    public interface IRepository<out T> where T: class
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        T SelectInformation(SqlDataReader reader);
    }
}
