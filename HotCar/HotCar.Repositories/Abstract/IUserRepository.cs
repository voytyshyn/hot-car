﻿using System;
using System.Collections.Generic;
using HotCar.Entities;
using HotCar.Entities.Enums;

namespace HotCar.Repositories.Abstract
{
    public interface IUserRepository
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        List<User> GetAllUsers();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        List<User> GetUsersByRole(UserRoles role);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        User GetUserById(int id);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        User GetUserByLogin(String login);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        User GetAccountInfo(String login);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        User GetDriverByTripId(int id);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        List<User> GetAllPassengerByTripId(int id);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="firstName"></param>
        /// <returns></returns>
        List<User> GetUsersByFirstName(String firstName);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="surName"></param>
        /// <returns></returns>
        List<User> GetUsersBySurName(String surName);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="surName"></param>
        /// <param name="firstName"></param>
        /// <returns></returns>
        List<User> GetUsersBySurFirstName(String surName, String firstName);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        UserRoles GetUserRoleByLogin(String login);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        UserRoles GetUserRoleById(int userId);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        int GetUsersNumber();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        byte[] GetUserPasswordByLogin(String login);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        string GetUserLoginById(int id);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        UserPhoto GetUserPhoto(int userId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="login"></param>
        /// <param name="file"></param>
        /// <param name="fileExtension"></param>
        /// <returns></returns>
        bool UploadUserPhoto(string login, byte[] file, string fileExtension);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        bool InsertUser(User user);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        bool UpdateUserByLogin(User user);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        bool UpdateUserById(User user);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool DeleteUserById(int id);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        bool DeleteUserByLogin(String login);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool LockUserById(int id);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool UnlockUserById(int id);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mail"></param>
        /// <returns></returns>
        bool IsMailValid(string mail);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        bool IsLoginValid(string login);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="phone"></param>
        /// <returns></returns>
        bool IsPhoneValid(string phone);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        bool UpdateUserPassword(int id, string password);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        List<User> GetInactiveUsers();
    }
}
